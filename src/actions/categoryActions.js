import { FETCH_CATEGORIES } from './types';

import axios from 'axios';

export function fetchCategories() {
  return function(dispatch) {
    axios.get('http://myapp-env.9rvrj23dbu.ap-southeast-1.elasticbeanstalk.com/api/v1/categories')
          .then(categories => dispatch({
            type: FETCH_CATEGORIES,
            payload: categories.data
          }))
          .catch(function(error) {
            console.log(error);
          });
  }
}
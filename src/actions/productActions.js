import { FETCH_PRODUCTS } from './types';
import { FETCH_PRODUCT_DETAILS } from './types';
import { FETCH_PRODUCTS_BY_PRICE } from './types';

import axios from 'axios';

export function fetchProducts(object) {
  return function(dispatch) {
    axios.get('http://myapp-env.9rvrj23dbu.ap-southeast-1.elasticbeanstalk.com/api/v1/products',
          {
            params: object
          })
          .then(products => dispatch({
            type: FETCH_PRODUCTS,
            payload: products.data
          }))
          .catch(function(error) {
            console.log(error);
          });
  }
}

export function fetchProductDetails(params) {
  return function(dispatch) {
    axios.get('http://myapp-env.9rvrj23dbu.ap-southeast-1.elasticbeanstalk.com/api/v1/products/' + params.productId)
          .then(product => dispatch({
            type: FETCH_PRODUCT_DETAILS,
            payload: product.data
          }))
          .catch(function(error) {
            console.log(error);
          });
  }
}

export function fetchProductsByPrice(object) {
  return function(dispatch) {
    axios.get('http://myapp-env.9rvrj23dbu.ap-southeast-1.elasticbeanstalk.com/api/v1/products/sort_by_price',
          {
            params: object
          })
          .then(products => dispatch({
            type: FETCH_PRODUCTS_BY_PRICE,
            payload: products.data
          }))
          .catch(function(error) {
            console.log(error);
          });
  }
}
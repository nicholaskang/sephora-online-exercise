import React, { Component } from 'react';
import Header from './components/header.js';
import store from './components/store.js';
import { Provider } from 'react-redux';
import ProductDetails from './components/ProductDetails.js';


import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';
import HomePage from './components/HomePage.js';

class App extends Component {
  
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div className='App'>
            <div className="container-fluid">
              <div className="row">
                <Header />
              </div>

            </div>
            <div className="container">
              
              <div className='row'>
                <Route path='/' exact={true} component={HomePage} />
                <Route path="/products/:productId" component={ProductDetails} />
              </div>
            </div>
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;

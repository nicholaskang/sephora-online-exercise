import { combineReducers } from 'redux';
import productsReducer from './productsReducer.js';
import categoriesReducer from './categoriesReducer.js';

export default combineReducers({
    productsReducer: productsReducer,
    categoriesReducer: categoriesReducer
});
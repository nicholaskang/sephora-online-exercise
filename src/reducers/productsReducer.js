import { FETCH_PRODUCTS } from '../actions/types.js';
import { FETCH_PRODUCT_DETAILS } from '../actions/types.js';
import { FETCH_PRODUCTS_BY_PRICE } from '../actions/types.js';

const initialState = {
    productListings: [],
    product: {}
}

export default function(state = initialState, action) {
    switch(action.type) {
        case FETCH_PRODUCTS:
            return {
                productListings: action.payload
            }

        case FETCH_PRODUCTS_BY_PRICE:
            return {
                productListings: action.payload
            }
            
        case FETCH_PRODUCT_DETAILS:
            return {
                product: action.payload
            }
        
        default:
            return state;
    }
}
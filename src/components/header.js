import React from 'react';

export default class Header extends React.Component {
  render() {
    return (
      <div className='top-navigation'>
        <div className='container'>
            <h1>Happy eCommerce</h1>
        </div>
      </div>
    );
  }
}
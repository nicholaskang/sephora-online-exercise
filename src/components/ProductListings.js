import React from 'react';
import Product from './Product.js'
import { connect } from 'react-redux';
import { fetchProducts } from '../actions/productActions.js';
import JwPagination from 'jw-react-pagination';

class ProductListings extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      pageOfItems: []
    };

    this.onChangePage = this.onChangePage.bind(this);
  }

  componentWillMount() {
    console.log('fetching')
    this.props.fetchProducts();
  }

  onChangePage(pageOfItems) {
    this.setState({ pageOfItems: pageOfItems });
  }

  render() {
    var currentProducts;

    if(this.state.pageOfItems.length > 0) {
      currentProducts = this.state.pageOfItems
    } else if(this.state.pageOfItems.length === 0) {
      currentProducts = this.props.products
    }

    if (this.props.products === undefined) {
      return (
        <div></div>
      );
    }

    if(this.props.products.length === 0) {
      return (
        <div></div>
      );
    }

    return (
      <div>
        <div className='product-listings-wrapper'>
          <div className='row'>
            <div className='col-sm-12'>
              <JwPagination items={this.props.products} onChangePage={this.onChangePage} />
            </div>
          </div>
          <div className='row'>
            { currentProducts.map((product) => {
                return (
                  <Product product={product} key={product.id} />
                );
              })
            }
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  products: state.productsReducer.productListings
});

export default connect(mapStateToProps, { fetchProducts })(ProductListings);

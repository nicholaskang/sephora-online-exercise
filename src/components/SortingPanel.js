import React from 'react';
import SortByPrice from './SortByPrice.js';

export default class SortingPanel extends React.Component {
  render() {
    return (
        <div>
            <div className='sorting-panel'>
                <div className='row'>
                    <div className='col-sm-12'>
                        <div className='sorting-types'>
                            <h4>PRICE</h4>
                            <i className='fas fa-caret-down'></i>
                        </div>
                        <SortByPrice/>
                    </div>
                </div>
            </div>
        </div>
    );
  }
}



import React from 'react';
import { connect } from 'react-redux';
import { fetchProductDetails } from '../actions/productActions.js';
import { Link } from 'react-router-dom';

class ProductDetails extends React.Component {

  componentWillMount() {
    const { match: { params } } = this.props;
    this.props.fetchProductDetails(params);
  }

  displayPrice(number) {
    return (number/100).toFixed(2);
  }

  render() {
    const product = this.props.product;

    if(product){
      if((!product || Object.keys(product).length === 0) && product.constructor === Object) {
        return (
          <div className='container'>
            <div className='row col-sm-12'>
                This product does not exist or has been sold out
            </div>
          </div>
        );
      }
    }

    if(product === undefined) {
      return(
        <div></div>
      )
    }

    return (
      <div className='container'>
        <Link to={this.props.location.from ? this.props.location.state.from : '/'}>
          Back to All Products
        </Link>
        <div className='row'>
          <div className='col-sm-6'>
            <img
              alt=''
              src={product.image_source}
              style={{width: '100%'}}/>
          </div>
          <div className='col-sm-6 product-details'>
            <h3>{product.name}</h3>
            <p>{product.description}</p>
            <p className='fake-stars'>★★★★★</p>
            <br/><br/>
            <p className='price'>${this.displayPrice(product.sale_price)}</p>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  product: state.productsReducer.product
});

export default connect(mapStateToProps, { fetchProductDetails })(ProductDetails);

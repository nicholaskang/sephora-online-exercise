import React from 'react';
import { connect } from 'react-redux';
import { fetchCategories } from '../actions/categoryActions.js';
import { fetchProducts } from '../actions/productActions.js';

class CategoryNavigation extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedOption: ''
    };
    
    this.handleButtonClick=this.handleButtonClick.bind(this);
  }

  componentWillMount() {
    this.props.fetchCategories();
    this.resetRadioButtons();
  }

  handleButtonClick(event) {
    event.preventDefault();
    
    let element = event.target;
    let value = element.getAttribute('value');
    
    window.location.hash = element.getAttribute('href');
    this.styleLinks(element);

    this.setState({
      selectedOption: value
    });
    this.props.fetchProducts({category: value});
    
    this.resetRadioButtons();
  }

  setIntervalX(callback, delay, repetitions) {
    var x = 0;
    var intervalID = window.setInterval(function () {

       callback();

       if (++x === repetitions) {
           window.clearInterval(intervalID);
       }
    }, delay);
  }

  styleLinks(element) {
    document.querySelectorAll('a.category-names').forEach(label=> {
      label.classList.remove('bold');
    });

    element.classList.add('bold');
  }

  resetRadioButtons() {
    let radioList = document.querySelectorAll('input[type="radio"]');

    this.setIntervalX(function() {
      Array.prototype.forEach.call(radioList, function (item) {
        if (item.checked) {
          item.checked = false;
        }
      });
    }, 8, 10)
    
  }

  render() {
    return (
      <div>
        <ul className='category-listings'>
          <li>    
            <a href='/' className='category-names' onClick={this.handleButtonClick}>
              All Products
            </a>
          </li>
        { this.props.categories.map((category) => {
          return(
            <li key={category.id}>
              <a href={`#${category.name}`} value={category.name} onClick={this.handleButtonClick} className='category-names'>
                {category.name}
              </a>
            </li>
          );
        })}
        </ul>
        <hr/>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  categories: state.categoriesReducer.categories
});

export default connect(mapStateToProps, { fetchCategories, fetchProducts })(CategoryNavigation);
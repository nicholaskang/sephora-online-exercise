import React from 'react';
import { connect } from 'react-redux';
import { fetchProductsByPrice } from '../actions/productActions.js';

class SortByPrice extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            selectedOption: ''
        }

        this.handleSelectedOption = this.handleSelectedOption.bind(this);
    }
    
    handleSelectedOption(e) {
        this.setState({
            selectedOption: e.target.value
        })
        
        this.props.fetchProductsByPrice({
            category: window.location.hash.slice(1),
            price_filter: e.target.value
        });
    }

    render() {
        return (
            <div style={{margin: '20px 0 0 20px'}}>
                <form>
                    <label>
                        <input
                            type="radio"
                            value="0-2500"
                            name="filter-price-radio"
                            checked={this.state.selectedOption==='0-2500'}
                            onChange={this.handleSelectedOption}/>
                        <p className='filter-price'>Under $25</p>
                    </label>
                    <br/><br/>
                    <label>
                        <input
                            type="radio"
                            value="2500-5000"
                            name="filter-price-radio"
                            checked={this.state.selectedOption==='2500-5000'}
                            onChange={this.handleSelectedOption}/>
                        <p className='filter-price'>$25 - $50</p>
                    </label>
                    <br/><br/>
                    <label>
                        <input
                            type="radio"
                            value="5000-10000"
                            name="filter-price-radio"
                            checked={this.state.selectedOption==='5000-10000'}
                            onChange={this.handleSelectedOption}/>
                        <p className='filter-price'>$50 - $100</p>
                    </label>
                    <br/><br/>
                    <label>
                        <input type="radio"
                            value="10000-15000"
                            name="filter-price-radio"
                            checked={this.state.selectedOption==='10000-15000'}
                            onChange={this.handleSelectedOption}/>
                        <p className='filter-price'>$100 - $150</p>
                    </label>
                    <br/><br/>
                    <label>
                        <input
                            type="radio"
                            value="15000-30000"
                            name="filter-price-radio"
                            checked={this.state.selectedOption==='15000-30000'}
                            onChange={this.handleSelectedOption}/>
                        <p className='filter-price'>$150 - $300</p>
                    </label>
                    <br/><br/>
                    <label>
                        <input
                            type="radio"
                            value="30000-99999999"
                            name="filter-price-radio"
                            checked={this.state.selectedOption==='30000-99999999'}
                            onChange={this.handleSelectedOption}/>
                        <p className='filter-price'>Above $300</p>
                    </label>
                </form>
            </div>
        );
    }
}


const mapStateToProps = state => ({
    products: state.productsReducer.productListings
});

export default connect(mapStateToProps, { fetchProductsByPrice })(SortByPrice);
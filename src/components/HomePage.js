import React from 'react';
import ProductListings from './ProductListings.js';
import SortingPanel from './SortingPanel.js';
import CategoryNavigation from './CategoryNavigation.js';

export default class HomePage extends React.Component {

  componentWillMount() {
    
  }
  render() {
    return (
        <div className="container">
          <div className="row">
            <div className="col-sm-12">
              <CategoryNavigation />
            </div>
          </div>
          <div className="row">
            <div className="col-sm-3">
              <SortingPanel />
            </div>
            <div className="col-sm-9">
              <ProductListings />
            </div>
          </div>
        </div>
    );
  }
}



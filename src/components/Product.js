import React from 'react';
import { Link } from 'react-router-dom';

export default class Product extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showActions: false
    }

    this.showActions=this.showActions.bind(this);
    this.hideActions=this.hideActions.bind(this);
    this.handleButtonClick=this.handleButtonClick.bind(this);
  }

  showActions() {
    this.setState({showActions: true});
  }

  hideActions() {
    this.setState({showActions: false});
  }

  handleButtonClick() {
  }

  displayPrice(number) {
    return (number/100).toFixed(2);
  }

  render() {
    const product = this.props.product;
    const productUrl = product.name === undefined ? '' : product.name.split(' ').join('-');
    
    return (
      <div className='col-sm-4'>
        <Link to={{
          pathname: `/products/${product.id}/${productUrl}`,
          state: { from: window.location.pathname }
         }}>
          <div
            className='product-card'
            onMouseEnter={this.showActions}
            onMouseOut={this.hideActions}>
            
            <div
              className='heart'
              onMouseEnter={this.showActions}
              style={this.state.showActions ? {visibility: 'visible'} : {visibility: 'hidden'}}>
            </div>
            <img src={product.image_source}
                alt=''
                onMouseEnter={this.showActions}
                style={{width: '100%'}}/>

            <div style={this.state.showActions ? {visibility: 'visible'} : {visibility: 'hidden'}}>
              <button
                className='product-card-actions'
                onClick={this.handleButtonClick}
                onMouseEnter={this.showActions}
                style={product.sold_out ?
                      {backgroundColor: 'black'} :
                      {backgroundColor: '#d50032'}}>

                {product.sold_out ? 'waitlist me' : 'add to bag'}
              </button>
            </div>

            <p
              onMouseEnter={this.showActions}
              className='product-card-brand'>{product.name}
            </p>
            <p
              onMouseEnter={this.showActions}
              className='product-card-description'>{product.description}
            </p>
            <p
              onMouseEnter={this.showActions}
              className='product-card-price'>${this.displayPrice(product.price)}
            </p>
            
          </div>
        </Link>
      </div>
    );
  }
}
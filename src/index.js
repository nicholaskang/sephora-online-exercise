import React from 'react';
import ReactDOM from 'react-dom';
import './assets/css/src/index.css';
import './assets/css/vendors/bootstrap-grid.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));

registerServiceWorker();
